#!bin/bash

TEST_STATUS="Run is created with Run Number: 1-66 Run Result: Pass - 1 Fail - 0 Abort - 0"
echo $TEST_STATUS
FAIL_COUNT=$(echo $TEST_STATUS | sed -e 's/.*Fail - \(.*\) Abort.*/\1/')
echo $FAIL_COUNT
if [[ $FAIL_COUNT > 0 ]]; then
	echo 1
fi
